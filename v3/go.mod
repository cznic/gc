module modernc.org/gc/v3

go 1.22.0

toolchain go1.23.4

require (
	github.com/dustin/go-humanize v1.0.1
	github.com/hashicorp/golang-lru/v2 v2.0.7
	github.com/pmezard/go-difflib v1.0.0
	golang.org/x/exp v0.0.0-20241217172543-b2144cdd0a67
	golang.org/x/tools v0.28.0
	modernc.org/ebnfutil v1.1.0
	modernc.org/mathutil v1.7.0
	modernc.org/strutil v1.2.0
	modernc.org/token v1.1.0
)

require (
	github.com/remyoudompheng/bigfft v0.0.0-20230129092748-24d4a6f8daec // indirect
	golang.org/x/mod v0.22.0 // indirect
	golang.org/x/sync v0.10.0 // indirect
	modernc.org/ebnf v1.1.0 // indirect
)
